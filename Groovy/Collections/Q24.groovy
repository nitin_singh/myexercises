/*
Q24)Write a method which retruns the value of passed key from a search string of the form "http://www.google.com?name=johny&age=20&hobby=cricket"
*/

def findKeys = { string -> string.tokenize('?')[1].tokenize('&').each { println it.tokenize('=')[0] } }

findKeys("http://www.google.com?name=johny&age=20&hobby=cricket")
