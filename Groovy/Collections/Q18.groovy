/*
Q18)Create a new map by adding two existing maps
*/

Map map1 = ["books": 51, "pens": 100, "cards": 200]
Map map2 = ["car": 1, "bike": 2, "scooter": 3, "cycle": 100]
Map map3 = map1 + map2
println map3

