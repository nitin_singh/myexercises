/*
Q2)Create a set from a list containing duplicate elements. 
What do you observe? How can you achieve the same result without converting a list to a set? 
*/

List list = [1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 3, 3, 3, 3, 3, 4, 4, 4, 4, 4, 5, 5, 5, 5, 5]
Set set = list as Set


print "list is:"
println list

print "Set is: "
println set

//Observation: Only unique elements are stored after creating Set (which is the property of set).

//Another way to get unique elements from a list

//List l1=list.unique() //changes the original list
//List l1=list.unique(true) //changes the original list
List l1 = list.unique(false)

println "Original List: ${list}"
println "List l: ${l1}"
