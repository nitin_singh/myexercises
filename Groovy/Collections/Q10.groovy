/*
Q10) Get first, second and last element of Range.
*/

Range range = 1..1000
println "First Element:" + range.first()
println "Second Element:" + range.get(1)
println "Last Element:" + range.last()

