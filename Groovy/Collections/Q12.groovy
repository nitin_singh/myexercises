/*
Q12)We have a sorted list of alphabets a-z, print all alphabets appearing after j
*/

List alphabets = 'a'..'z'

print alphabets.findAll { it > 'j' }