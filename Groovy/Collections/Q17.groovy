/*
Q17)Iterate over the previous map in as many ways as possible

Previous map: 
Map friends=["Pooja":21,"Surabhi":26,"Saksham":26,"Saloni":24,"Sakshi":24,"Surbhi":24,"Shalika":24,"Shreya":21,"Akash":23,"Anushree":24]
*/


Map friends = ["Pooja": 21, "Surabhi": 26, "Saksham": 26, "Saloni": 24, "Sakshi": 24, "Surbhi": 24, "Shalika": 24, "Shreya": 21, "Akash": 23, "Anushree": 24]

//using each method
friends.each { entry ->
    println "Name: $entry.key Age: $entry.value"
}

//using eachWithIndex method
friends.eachWithIndex { entry, index ->
    println "$index - Name: $entry.key Age: $entry.value"
}

friends.each { key, value ->
    println "Name: $key Age: $value"
}

// Key, value and i as the index in the map
friends.eachWithIndex { key, value, i ->
    println "$i - Name: $key Age: $value"
}