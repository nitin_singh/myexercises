/*
Q19)Try the following code on a map: 
println map.class println map.getClass() 
What do you observe?
*/

Map map = ["books": 51, "pens": 100, "cards": 200]

println map.class  //searches for a key with name "class" and returns null if not found
println map.getClass() //returns the class of map which is LinkedHashMap
