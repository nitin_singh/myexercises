/*
Q21)Find if a map contains a particular key
*/
Map friends = ["Pooja": 21, "Surabhi": 26, "Saksham": 26, "Saloni": 24, "Sakshi": 24, "Surbhi": 24, "Shalika": 24, "Shreya": 21, "Akash": 23, "Anushree": 24]
println friends.containsKey("Nitin")
println friends.containsKey("Pooja")