/*
Q23)Conside a class named "Employee" which has the following properties:
1) Name 2) Age 3) DepartmentName 4) EmployeeNumber 5) Salary
Let's say that there's a list of 50 employees available.
Perform the following operations on the list of employees:
a) Group the employees on the basis of the bracket in which their salary falls. The ranges are 0-5000, 5001 and 10000, and so on.
b) Get a count of the number of employees in each department
c) Get the list of employees whose age is between 18 and 35
d) Group the employees according to the alphabet with which their first name starts
and display the number of employees in each group whose age is greater than 20
e) Group the employees according to their department
*/

class Employee {
    String name
    int age
    String deptName
    int empNumber
    int salary
    
    String toString() {
        "Name:$name, Age:$age, Department Name:$deptName, EmployeeNumber:$empNumber, Salary:$salary\n"
    }
}

List<Employee> list = new ArrayList()

10.times {
    list.add(new Employee(name: "aEmployee" + (it + 1), age: 20, deptName: "Department" + (it + 1), empNumber: it + 100, salary: it * 100))
}

10.times {
    list.add(new Employee(name: "bEmployee" + (it + 11), age: 40, deptName: "Department" + (it + 1), empNumber: it + 1000, salary: it * 1000))
}

10.times {
    list.add(new Employee(name: "cEmployee" + (it + 21), age: 50, deptName: "Department" + (it + 1), empNumber: it + 10000, salary: it * 10000))
}

10.times {
    list.add(new Employee(name: "dEmployee" + (it + 31), age: 60, deptName: "Department" + (it + 1), empNumber: it + 100000, salary: it * 2000))
}

10.times {
    list.add(new Employee(name: "eEmployee" + (it + 41), age: 36, deptName: "Department" + (it + 1), empNumber: it + 2000000, salary: it * 3000))
}

//part a) Group the employees on the basis of the bracket in which their salary falls. The ranges are 0-5000, 5001 and 10000, and so on.

Map salaryBracket = list.groupBy {
    if (it.salary > 0 && it.salary <= 5000)
        "0-5000"
    else if (it.salary > 5000 && it.salary <= 10000)
        "5001-10000"
    else if (it.salary > 10000 && it.salary <= 15000)
        "10001-15000"
    else if (it.salary > 15000 && it.salary <= 20000)
        "15001-20000"
    else
        ">20000"
}

println salaryBracket

//part b Get a count of the number of employees in each department
Map departments = list.groupBy { it.deptName }
departments.each { entry -> println "$entry.key , Count: ${entry.value.count { it }}" }

//part c) Get the list of employees whose age is between 18 and 35
println "Emloyees aged between 18 and 35"
list.each { if (it.age >= 18 && it.age <= 35) println it }

/*
part d) Group the employees according to the alphabet with which their first name starts
and display the number of employees in each group whose age is greater than 20*/
/*Map map = list.groupBy { it.name[0].each {} }

int count = 0
map.each {
    it.value.each {
        if (it.age > 20) {
            count++
        }
    }
    println "${it.key}:${count}"
    count = 0
}*/

alphabetGroupByList=list.groupBy{ emp -> emp.name[0]}
println alphabetGroupByList
alphabetGroupByList.each{ key,value ->println "$key:${value.findAll{it.age>20}.size()}"}

//part e)Group the employees according to their department
Map departmentGroup = list.groupBy { it.deptName }
println departmentGroup




















