/*
Q1. Initialize a list using a range and find all elements which are even.
*/

List l = 1..20
printf "Even elements are:"
print l.findAll { it % 2 == 0 }