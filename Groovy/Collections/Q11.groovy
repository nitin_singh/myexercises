/*Q11) Print the table of a given number : 2 and 12
*/

println "Table of 2"
(1..10).each { print it * 2 + "  " }
println ""

println "Table of 12"
(1..10).each { print it * 12 + "  " }