/*
Q22)Consider the following map:
Map m = [‘Computing’ : [‘Computing’ : 600, ‘Information Systems’ : 300], ‘Engineering’ : [‘Civil’ : 200, ‘Mechanical’ : 100], ‘Management’ : [‘Management’ : 800] ]
22a) How many university departments are there?
22b) How many programs are delivered by the Computing department? 
22c) How many students are enrolled in the Civil Engineering program?
*/

Map map = ["Computing": ["Computing": 600, "Information Systems": 300], "Engineering": ["Civil": 200, "Mechanical": 100], "Management": ["Management": 800]]

//part a
println "Total Departments: " + map.count { it }

//part b
println "Programs Delivered by Computing Department: " + map['Computing'].count { it }

//part c
println "Students in Civil Engineering Department: " + map['Engineering']['Civil']