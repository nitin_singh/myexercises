/*
Q4)Find whether two lists have a common element or not 
*/

List list1 = [1, 2, 3, 4, 5]
List list2 = [5, 6, 7, 8, 9]
list list3 = [6, 7, 8, 9, 0]
def common = { lista, listb ->
    if (lista.intersect(listb))
        println "Common Elements:" + lista.intersect(listb)
    else
        println "No common elements"
}

common(list1, list2)
common(list1, list3)