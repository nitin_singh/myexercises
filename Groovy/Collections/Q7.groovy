/*
Q7)Sort the given list in descending order having distinct elements: [14,12, 11,10, 16, 15,12, 10, 99, 90, 14, 16, 35]
*/
List list = [14, 12, 11, 10, 16, 15, 12, 10, 99, 90, 14, 16, 35]

List sortedUniqueList = list.sort(false, { -it }).unique(false) //false for not making changes to the original list
println list
print sortedUniqueList