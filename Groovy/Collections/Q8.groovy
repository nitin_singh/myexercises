/*
8. Consider a class Employee with following details * Name * Age * Salary Create a list consisting of 10 Employee objects. 
8(a). Get a list of employees who earn less than 5000 
8(b). Get the name of the youngest employee and oldest employee 
8(c).Get the employee with maximum salary 
6(d). Get the list of names of all the employees
*/

class Employee {
    String name
    int salary
    int age
}

Employee emp1 = new Employee(name: "Krishna", salary: 123456, age: 24)
Employee emp2 = new Employee(name: "Raghav", salary: 123448, age: 25)
Employee emp3 = new Employee(name: "Ram", salary: 123447, age: 26)
Employee emp4 = new Employee(name: "Shilpa", salary: 123446, age: 27)
Employee emp5 = new Employee(name: "Shiv", salary: 123445, age: 28)
Employee emp6 = new Employee(name: "Ajay", salary: 4999, age: 29)
Employee emp7 = new Employee(name: "Rohit", salary: 123, age: 30)
Employee emp8 = new Employee(name: "Abhishek", salary: 123442, age: 31)
Employee emp9 = new Employee(name: "Pooja", salary: 123441, age: 32)
Employee emp10 = new Employee(name: "Priya", salary: 123440, age: 33)
List employeeList = [emp1, emp2, emp3, emp4, emp5, emp6, emp7, emp8, emp9, emp10]

//Part a
List employeeSalaryList = employeeList.findAll { it.salary < 5000 }

println "Employees with salary<5000: " + employeeSalaryList*.name

//Part b
println "Youngest Employee :" + employeeList.max { -it.age }.name
println "Oldest Employee :" + employeeList.min { -it.age }.name

//Part c
println "Employee With maximum salary: " + employeeList.max { it.salary }.name

//Part d
println "List of all employees :" + employeeList*.name
