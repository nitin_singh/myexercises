/*
Q20)Consider the following map: Map m = ['1' : 2, '2' : 3, '3' : 4, '2':5] 
Is this a valid construction? What is the value of m['2']?
*/

Map m = ['1': 2, '2': 3, '3': 4, '2': 5] //yes his is a valid construction
println m['2']  // will return value for key '2' i.e 5