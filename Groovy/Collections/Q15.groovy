/*
Q15)Consider a class named "Stack" that holds a list of objects and has the following operations associated:
1) POP - Pops the last element off the stack
2) PUSH - Pushes an element on top of the stack
3) TOP - Returns the element at the top of the list
Implement the aforesaid class
*/

class Student {
    String name
    int age
    
    String toString() {
        "Name: $name, Age: $age"
    }
}

class Stack {
    List<Student> list
    
    Stack() {
        list = new ArrayList()
    }
    
    void push(Student student) {
        list.add(student)
    }

    Student pop(Student student) {
        list.pop()
    }
    
    Student top() {
        list.head()
    }
    
    void show() {
        list.each { println it }
    }
}

Stack students = new Stack()
students.push(new Student(name: "Nitin", age: 24))
students.push(new Student(name: "Shalika", age: 24))
students.push(new Student(name: "Sakshi", age: 24))
students.push(new Student(name: "Saksham", age: 24))
students.show()

println "Popped: " + students.pop()
students.show()

println "Top: " + students.top()


