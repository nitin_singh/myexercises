/*
Q3) Given two lists [11, 12, 13, 14] and [13, 14, 15],
how would we obtain the list of items from the first that are not in the second? 
*/

List list1 = [11, 12, 13, 14]
List list2 = [13, 14, 15]

print "List Difference: ${list1 - list2}"