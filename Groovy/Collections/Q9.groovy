/*
Q9)9. Consider the following piece of code: 
String s = "this string needs to be split" println s.tokenize(" ") println s.tokenize() 
Compare this with the following code: 
String s = "this string needs to be split" println s.split(" ") println s.split(/\s/) 
(Try Same Parameter with tokenize) Also try the following exercise: 
String s = "are.you.trying.to.split.me.mister?" s.tokenize(".") s.split(".")
*/


String string1 = "this string needs to be split"
println "Using tokenize"
println string1.tokenize(" ")
println string1.tokenize()

String string2 = "this string needs to be split"
println "Using split"
println string2.split(" ")
println string2.split(/\s/)

println "Using tokenize"
println string1.tokenize()
println string1.tokenize(/\s/)

/*
Both methods return same output but the return type of tokenize is an ArrayList 
whereas the return type of split method is an array of Strings.
split can take regular expressions as arguments wheres as tokenize takes the delimiters.

The tokenize() method uses each character of a String as delimeter 
where as split()  takes the entire string as  delimeter
*/

/*String string1 = "this string needs to be split"
println string1.tokenize(" ").getClass()
println string1.tokenize().getClass() 

String string2 = "this string needs to be split"
println string2.split(" ").getClass() 
println string2.split(/\s/).getClass()
*/


String s = "are.you.trying.to.split.me.mister?"
println "***************"
println s.tokenize(".")
println s.split(".") 
 
 
 