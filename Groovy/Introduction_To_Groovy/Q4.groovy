/*Question 4
GString... override the toString() of the Person class to return something like
"Sachin is a man aged 24 who lives at Delhi. He works for Intelligrape with employee id 12 and draws $$$$$$$ lots of money !!!!." 
*/

class Employee extends Person {
    String empId
    int salary
    String company

    String toString() {
        """
    Sachin is a ${gender} aged ${age} who lives at ${address}.
    He works at ${company} with employee id ${empId} and 
    draws ${salary} lots of money. """
    }
}

Employee obj = new Employee();

obj.name = "Sachin"
obj.age = 24
obj.address = "Delhi"
obj.gender = "man"
obj.empId = "IG12"
obj.company = "Intelligrape"
obj.salary = 12345

print "$obj"

/*
println "${obj}"


println new Person().getClass()
println "".getClass()
*/



