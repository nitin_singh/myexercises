/*
Q5
5. Groovy Truth: if('test') { printlnn "test evaluated to true inside if" } 
try replacing test with various objects and observe its behaviour. 
a) "Test" 
b)'null' 
c) null 
d) 100 
e) 0 
f) empty list 
g) list with all vaues as null List list = new ArrayList() 
*/

//a)
if ("Test")
    println "a) returned true"

//b)
if ('null')
    println "b) returned true"

//c)
if (null)
    println "c) returned true"
else println "c) returned false"

//d)
if (100)
    println "d) returned true"
else println "d) returned false"

//e)
if (0)
    println "e) returned true"
else println "e) returned false"

//f)
if ([])
    println "f)EmptyList returned true"
else println "f)Empty List returned false"

//g)
List list = new ArrayList()
list.add(null)
list.add(null)
list.add(null)

if (list)
    println "g)List returned true"
else println "g)List returned false"






