package Introduction_To_Groovy;

public class Person {
    protected String name, gender, address;
    protected int age;

    //getters
    String getName() {
        return name;
    }

    //setters
    void setName(String name) {
        this.name = name;
    }

    String getGender() {
        return gender;
    }

    void setGender(String gender) {
        this.gender = gender;
    }

    String getAddress() {
        return address;
    }

    void setAddress(String address) {
        this.address = address;
    }

    int getAge() {
        return age;
    }

    void setAge(int age) {
        this.age = age;
    }

}
