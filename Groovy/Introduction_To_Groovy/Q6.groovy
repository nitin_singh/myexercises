/*
Q6) Write a class HourMinute where the class stores hours and minutes as separate fields.
Overload + and - operator operator for this class
*/

class HourMinute {
    int minutes
    int hours
    
    HourMinute plus(HourMinute obj) {
        HourMinute tmp = new HourMinute()
        tmp.hours = this.hours + obj.hours
        tmp.minutes = this.minutes + obj.minutes
        tmp
    }
    
    HourMinute minus(HourMinute obj) {
        HourMinute tmp = new HourMinute()
        tmp.hours = this.hours - obj.hours
        tmp.minutes = this.minutes - obj.minutes
        tmp
    }
    
    String toString() {
        "Time : ${hours} hrs. ${minutes} mins."
    }
}

HourMinute time1 = new HourMinute(hours: 6, minutes: 6)
HourMinute time2 = new HourMinute(hours: 5, minutes: 5)
HourMinute time3 = (time1 + time2)

println "${time3}"


HourMinute time4 = (time1 - time2)
println "${time4}"