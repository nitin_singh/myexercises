/*
Q7)Print multiple of 3 upto 10 terms in at least three different ways using groovy special methods 
*/

//method 1
println "*****METHOD 1*****"

1.upto(10) {
    println "3X${it}=${it * 3}"
}

//method 2
println "*****METHOD 2*****"

1.step 11, 1, {
    println "3X${it}=${it * 3}"
}

//method 3
println "*****METHOD 3*****"

10.times {
    println "3X${it + 1}=${it + 1 * 3}"
}