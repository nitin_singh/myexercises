/*
Q11) Write a method which removes all the white spaces in a file and writes the output to another file.
Suppose white space characters are Space, Tab and Enter
Tab:9
Space:32
Enter:10
*/

File destination = new File("withoutspaces.txt")

new File("whitespaces.txt").eachLine {
    line -> destination << line.replaceAll("\\s", "").replaceAll("\\n", "")
}