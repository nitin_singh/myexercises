/*
Q9)Combine content of all the files in a specific directory to another new file
*/

File combined = new File("/home/nitin/combined.txt")
combined.text = ''
File dir = new File("/home/nitin/stories")

dir.eachFile { file ->

    println file.name
    println "Starting Copying of ${file.name}"
    //combined.append("\n"+file.readLines()) //method 1 but content is enclosed in []
    combined.text += file.text   //method 2

}