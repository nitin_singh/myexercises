/*
Q8) Write a closure which checks if a value is contained within a list where the closure accepts two parameters
*/

def checkValue = { list, element -> println list.contains(element) }

List list1 = [1, 2, 3, 4, 5]
checkValue(list1, 2)

List list2 = new ArrayList([1, 2, 3, 4, 5])
checkValue(list2, 2)
list2 << 100
checkValue(list2, 100)


checkValue([1, 2, 3, 4, 5], 2)