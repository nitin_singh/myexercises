/*Question 3
Print Pattern
*
**
****
********

*/


0.upto(3) {
    
    int stars = Math.pow(2, Integer.parseInt("${it}"))
    
    1.upto(stars)
    print "* "

    println ""
}