/*
Q12)Make copy of an image type file byte by byte 
*/

File copy = new File("imagecopy.jpg")
File original = new File("image.jpg")
copy << original.bytes

/*or
new File('c:/temp/dst.zip') << new File('c:/temp/src.zip').bytes
*/