/*
Q10)Create a file which contains all the odd numbered lines of a given file.
Each line should be numbered at the beginning of line viz : 1, 3, 5.....
*/

int lineNum = 1;

File evenLinesFile = new File("EvenLines.txt")

new File("text.txt").eachLine {
    line ->

        if (lineNum % 2 == 0)
            evenLinesFile.append("${lineNum}.${line}\n")

        lineNum++
}
